#!/usr/bin/env python3
import time
import json
from http.server import HTTPServer, BaseHTTPRequestHandler

class Server(BaseHTTPRequestHandler):
    def do_POST(self):
        print("\nAlert Received\n")
        content_len = int(self.headers.get('Content-Length'))
        post_body = self.rfile.read(content_len)
        parsed = json.loads(post_body)
        print(json.dumps(parsed, indent=4))
        self.respond()

    def respond(self, status = 200):
        self.send_response(status)
        self.end_headers()


HOST_NAME = '0.0.0.0'
PORT_NUMBER = 8001

if __name__ == '__main__':
    httpd = HTTPServer(('0.0.0.0', PORT_NUMBER), Server)
    print(time.asctime(), 'Server UP - %s:%s' % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), 'Server DOWN - %s:%s' % (HOST_NAME, PORT_NUMBER))