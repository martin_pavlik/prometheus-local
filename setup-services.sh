#!/usr/bin/env bash

LOCAL_HOST="host.docker.internal"

print_help () {
    echo "Usage: ./setup-local-services.sh <service_name> <service_port> [<metrics_path>] [<external_host>]"
    echo "  service_name    - Name of the service (must be one word)"
    echo "  service_port    - port of the service running locally which exposes metrics"
    echo "  metrics_path    - Optional: URL path to metrics on the service. If not specifies, '/metrics' will be used instead"
    echo "  external_host   - Optional: host where the service runs. If not specified, it is expected that the service runs on local host."
    echo "./setup-local-services.sh BeaconService 1699 /metrics/prometheus"
}

# Validations
if [[ $1 == "--help" ]] || [[ $1 == "-h" ]]; then
    print_help
    exit 0
fi

if (( $# < 2 )) || (( $# > 4 )); then
    echo "error: Required number of arguments is 2 to 4. Provided number of arguments '$#'."
    print_help
    exit 1
fi

if (($2 < 1 || $2 > 65535)); then
    echo "error: Invalid port '$2'. Expected number between 1 and 65535."
    print_help
    exit 2
fi

path="/metrics"
# Use specified metrics path or fall back to /metrics
if [[ ! -z "$3" ]]; then
    path="$3"
fi

# If the metrics path is not prefixed with /, add the prefix
if [[ ${path} != '/'* ]]; then
    path="/$3"
fi

# Add .yml after the service name to get the target file
file="targets/$1"
if [[ ${file} != *'.yml' ]]; then
    file="$file.yml"
fi

# Use specified host or fall back to local one
host=${LOCAL_HOST}
if [[ ! -z "$4" ]]; then
    host="$4"
fi

hostPort="${host}:$2"

if [[ -f ${file} ]]; then
   echo "Updating scraper for service '$1' listening on '$hostPort$path' ('$file' file updated)"
else
   echo "Adding scraper for service '$1' listening on '$hostPort$path' ('$file' file added)"
fi

# Print the final yaml into the file where it is picked up by Prometheus
cat > ${file} <<- EOM
- targets: ["${hostPort}"]
  labels:
    __metrics_path__: ${path}
EOM

echo "For more details please see http://localhost:9090/targets"